from django.urls import path
from .views import index
from .views import friend_list

urlpatterns = [
    path('', index, name='index'),
    path('friends', friend_list, name='friends'),
    # TODO Add friends path using friend_list Views
]