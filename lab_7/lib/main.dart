import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  int _value = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('Homepage   Vaksin   Informasi   Layanan Pengguna   Donasi',
          style: TextStyle(
            color: Colors.grey,
            fontFamily: 'Times New Roman',
          ),
        ),
        backgroundColor: Colors.white,
      ),

      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                const Text('Customer Service\n',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Times New Roman',
                      fontSize: 30),
                ),

                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 20, 8,8),
                  child: TextFormField(
                    autofocus: true,
                    decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                ),

                Container(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: const Text('Silahkan pilih media untuk menghubungi kami: ', textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Times New Roman',
                        fontSize: 20),
                  )
                ),

                Row(children: <Widget>[
                  Radio(
                    value: 1,
                    groupValue: _value,
                    onChanged: (value){
                      setState(() {
                        _value = 1;
                      });
                    },
                  ),
                  SizedBox(width: 10.0,),
                  Text("Instagramㅤ‏‏‎ ‏‏‎"),
                ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),

                Row(children: <Widget>[
                  Radio(
                    value:2,
                    groupValue: _value,
                    onChanged: (value){
                      setState(() {
                        _value = 2;
                      });
                    },
                  ),
                  SizedBox(width: 10.0,),
                  Text("Twitter ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎‏‏‎‏‏‎"),
                ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),

                Row(children: <Widget>[
                  Radio(
                    value:3,
                    groupValue: _value,
                    onChanged: (value){
                      setState(() {
                        _value = 3;
                      });
                    },
                  ),
                  SizedBox(width: 10.0,),
                  Text("WA/Telepon‏‏‎ ‏‏‎"),
                ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),

                Row(children: <Widget>[
                  Radio(
                    value:4,
                    groupValue: _value,
                    onChanged: (value){
                      setState(() {
                        _value = 4;
                      });
                    },
                  ),
                  SizedBox(width: 10.0,),
                  Text("Semua‏‏‎ ‎‏‏‎ ‎‏‏‎ ‏‏‎ ‎‏‏‎ ‎‎‏‏‎ ‎ ‏‏‎ ‏‏‎  ‏‏‎ ‏‏‏"),
                ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),

                Padding(
                  padding: const EdgeInsets.all(8),
                  child: ElevatedButton(

                      child: const Text('Submit'),
                      onPressed: () {
                      },
                      style: ElevatedButton.styleFrom(
                          fixedSize: const Size(500, 50),
                          primary: Colors.grey,
                          textStyle: const TextStyle(
                            color: Colors.black,
                          )
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// Referensi: Charles Pramudana