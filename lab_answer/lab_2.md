1. Apakah perbedaan antara JSON dan XML?
- JSON adalah suatu format yang ditulis dalam bahasa JavaScript, sedangkan XML adalah bahasa markup yang memiliki tag yang diturunkan dari SGML (Standard Generalized Markup Language).
- JSON hanya mendukung tipe data primitif, seperti string, integer, boolean, dan array, sedangkan XML mendukung tipe data yang lebih rumit, seperti citra dan grafik.
- Pada JSON, data disimpan dalam bentuk map (pasangan key dan value), sementara pada XML, data disimpan dalam bentuk tree structure.
- JSON hanya support encoding UTF-8, sedangkan XML support berbagai tipe encoding.
- JSON mendukung penggunaan array, sedangkan XML tidak.
- Dari segi keamanan, XML lebih aman daripada JSON.
- JSON memiliki syntax yang sederhana dan mudah dimengerti sehingga membuat ukuran filenya cenderung lebih kecil daripada XML. Secara umum, XML memiliki ukuran file yang lebih besar daripada JSON dan sedikit lebih lebih rumit untuk dibaca dibandingkan dengan JSON.

2. Apakah perbedaan antara HTML dan XML?
- HTML: <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Bersifat tidak case-sensitive. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Jumlah tag yang bisa digunakan terbatas dan sudah ada aturan penggunaannya. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Fungsi utamanya untuk menampilkan data dan mengembangkan struktur web. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Closing tag tidak terlalu dibutuhkan. <br>

- XML: <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Bersifat case-senstitive. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Jumlah tag yang bisa digunakan tidak terbatas dan programmer bisa mendefinisikan tag-nya sendiri. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Fungsi utamanya untuk menyimpan dan bertukar data. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Closing tag wajib digunakan untuk menutup tag yang sudah digunakan. <br>
<br>
Referensi: <br>
https://www.geeksforgeeks.org/difference-between-json-and-xml <br>
https://www.monitorteknologi.com/perbedaan-json-dan-xml <br>
https://hackr.io/blog/json-vs-xml <br>
https://perbedaan.budisma.net/perbedaan-html-dan-xml.html <br>
https://www.upgrad.com/blog/html-vs-xml
