from .forms import FriendForm
from lab_1.models import Friend
from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')

def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')

def add_friend(request):
    form = FriendForm(request.POST or None)
    response2 = { 'form' : form }

    if form.is_valid() and request.method == "POST":
        print("valid")
        form.save()
        return HttpResponseRedirect('/lab-3')

    return render(request, 'lab3_form.html', response2)