import 'package:flutter/material.dart';

void main() {
  runApp( MaterialApp(
    theme: ThemeData(
      brightness: Brightness.light,
      fontFamily: 'Times New Roman',

    ),
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _State createState() => _State();

}

class _State extends State<MyApp> {
  TextEditingController namaController = TextEditingController();
  int _value = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Homepage   Vaksin   Informasi   Layanan Pengguna   Donasi',
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
          backgroundColor: Colors.white,
        ),

        body: Padding(
            padding: const EdgeInsets.all(30),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: const Text('Customer Service',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 22),
                    )),

                Container(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: const Text('Nama: '),
                ),

                Container(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: TextField(
                    controller: namaController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),

                Container(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: const Text(' Silahkan pilih media untuk menghubungi kami:', textAlign: TextAlign.center,),
                ),

                Row(children: <Widget>[
                    Radio(
                      value: 1,
                      groupValue: _value,
                      onChanged: (value){
                        setState(() {
                          _value = 1;
                        });
                      },
                    ),
                    SizedBox(width: 10.0,),
                    Text("Instagramㅤ‏‏‎ ‏‏‎"),
                    ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),

                Row(children: <Widget>[
                  Radio(
                    value:2,
                    groupValue: _value,
                    onChanged: (value){
                      setState(() {
                        _value = 2;
                      });
                    },
                  ),
                  SizedBox(width: 10.0,),
                  Text("Twitter ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎ ‏‏‎‏‏‎‏‏‎"),
                ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),

                Row(children: <Widget>[
                  Radio(
                    value:3,
                    groupValue: _value,
                    onChanged: (value){
                      setState(() {
                        _value = 3;
                      });
                    },
                  ),
                  SizedBox(width: 10.0,),
                  Text("WA/Telepon‏‏‎ ‏‏‎"),
                ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),

                Row(children: <Widget>[
                  Radio(
                    value:4,
                    groupValue: _value,
                    onChanged: (value){
                      setState(() {
                        _value = 4;
                      });
                    },
                  ),
                  SizedBox(width: 10.0,),
                  Text("Semua‏‏‎ ‎‏‏‎ ‎‏‏‎ ‏‏‎ ‎‏‏‎ ‎‎‏‏‎ ‎ ‏‏‎ ‏‏‎  ‏‏‎ ‏‏‏"),
                ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),

                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: ElevatedButton(
                        child: const Text('Submit'),
                        onPressed: () {
                        },
                        style:ElevatedButton.styleFrom(
                            primary: Colors.grey,
                            textStyle: const TextStyle(
                              color: Colors.black,
                            )
                        )
                    )
                ),

              ],
            )
        ),
    );
  }
}