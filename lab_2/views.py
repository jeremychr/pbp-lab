from .models import Note
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    data = Note.objects.all()
    response = {'data':data}
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")